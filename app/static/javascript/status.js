var current = 0;
var previous_values_of_retries = [];
var previous_values_of_commands = [];
var previous_values_of_channels = [];
var first_cycle_check = true;

$(document).ready(function () {
  setTimeout(refresh,50);

  function refresh(){
  var status_size = $(".status").length;
  if( current < status_size ){
    $.ajax({
    url : '/' + current + '/status',
    type : 'GET',
    async: true,
    success : function(data) {
        slow_for_red = false;
        status = data[0];
        retries = data[1];
        command = data[2];
        channel = data[3];

        if(status == 0){

//        var channel = $("div[name='channel " + current + "']").text();
//        var command = $("div[name='command " + current + "']").text();
          $("td[name=" + current + "]").find('img').attr('class','red');
          slow_for_red = true;
        }
        else if(status == 1){
        $("td[name=" + current + "]").find('img').attr('class','green');
        }

        if(first_cycle_check){
            previous_values_of_retries.push(retries);
            previous_values_of_channels.push(channel);
            previous_values_of_commands.push(command);
        }
        else{
        if(previous_values_of_retries[current] == 1 && retries == 0){
            $("td[id=" + current + "]").html('<a href="/start/' + current + '"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABuElEQVRoQ9WZzVGFMBDH2VCJxUAyw4UOpATsADvQDrSD5zE37UA7eFagnslMHDLE8aCQkGx2H3dgf7v//UqguvAHtuzXWl9LKR85M+4B2KqqTkKIoWmaL44gIQCL3Z/W2kEp9cQNIhTA230nhJg4RSMWYAE5W2t7pdQbh2gcAfB2T1LKW2qIFIDF9ldjTN913TsVSCqAS3AAmNq2vaeAyAHg7SYptzkBSMptbgAXDWvtQ13XY4lyiwKwauoMAEPbti+YuYEJUKTclgBw5XYdRbI3v1IAaOW2JICTFAA8A0CfK8GLA6yJkW26pQLI1vyoAdx0m1JuOQD87BpSypvYnsEJ4FC55QbgK9UYOt2yBPDldp7nYW/XYAvg5ykp5dVWXnAGCNovOAJENTluAEFe/y0pLgBRXucGkHRYRhmBpBHCR4EKIMnrlBLK4nUqgGxeLw2Atg+7kWOrTWutlwuOlAf9ABgLANXr2BJC9zoKwHLaEDL+pujxr3dzSOhyj9epvJ5DQqReTwU4GWPGvVUvt9b/+15MDhweeTFhQgGiFw1Mo2Mk9MH1hj5onC7lxZT/bEoo5cOl3v0G+gU6QFKA2VMAAAAASUVORK5CYII=" alt="Play" width="24" height="24"></a>');
            previous_values_of_retries[current] = 0;
        }
        else if(previous_values_of_retries[current] == 0 && retries == 1){
            $("td[id=" + current + "]").html('<a href="/stop/' + current + '" ><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAARklEQVRIS2NkoDFgBJm/c+fO/9S2x93dHWz2qAU4Q3Y0iAgmutEgGg0igiFAUMFoKhoNIoIhQFDBMEtFBP1LgQJwpU9LAABx9lQZ/QMCSQAAAABJRU5ErkJggg==" alt="Stop" width="24" height="24"></a>');
            previous_values_of_retries[current] = 1;
        }
        if(previous_values_of_channels[current] != channel){
            $('div[name="channel ' + current + '"]').text(channel);
            previous_values_of_channels[current] = channel;
        }

        if(previous_values_of_commands[current] != command){
            $('div[name="command ' + current + '"]').text(command);
            previous_values_of_commands[current] = command;
        }
        }

      current++;

      if(slow_for_red == false){
        setTimeout(refresh,50);
      }
      else if(slow_for_red == true){
        setTimeout(refresh,500);
      }
    },
    error : function(request,error)
    {
        location.reload(true);
    }
      });
    }
    else{
        current = 0;
        first_cycle_check = false;
        setTimeout(refresh,50);
    }
}
});