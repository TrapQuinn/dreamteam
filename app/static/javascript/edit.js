$('body').on('dblclick', '.channel', function(){

  var $el = $(this);

  var index = $el.attr("class").split(" ")[1];

  var $input = $('<input class="black">').val( $el.text() );
  $el.replaceWith( $input );

  var channel = "channelhidden " + index;

  var save = function(){
    var $p = $('<div name="channel ' + index + '" class="channel ' + index + ' dots">').text( $input.val() );
    $input.replaceWith( $p );

    $(document).ready(function () {
    $.ajax({
    url : '/edit/' + index,
    type : 'PUT',
    contentType: 'application/json',
    async: true,
    data: JSON.stringify({ 'channel': $input.val() }),
    success : function() {
        console.log("Updated!");
        $("td[id=" + index + "]").html('<a href="/start/' + index + '"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABuElEQVRoQ9WZzVGFMBDH2VCJxUAyw4UOpATsADvQDrSD5zE37UA7eFagnslMHDLE8aCQkGx2H3dgf7v//UqguvAHtuzXWl9LKR85M+4B2KqqTkKIoWmaL44gIQCL3Z/W2kEp9cQNIhTA230nhJg4RSMWYAE5W2t7pdQbh2gcAfB2T1LKW2qIFIDF9ldjTN913TsVSCqAS3AAmNq2vaeAyAHg7SYptzkBSMptbgAXDWvtQ13XY4lyiwKwauoMAEPbti+YuYEJUKTclgBw5XYdRbI3v1IAaOW2JICTFAA8A0CfK8GLA6yJkW26pQLI1vyoAdx0m1JuOQD87BpSypvYnsEJ4FC55QbgK9UYOt2yBPDldp7nYW/XYAvg5ykp5dVWXnAGCNovOAJENTluAEFe/y0pLgBRXucGkHRYRhmBpBHCR4EKIMnrlBLK4nUqgGxeLw2Atg+7kWOrTWutlwuOlAf9ABgLANXr2BJC9zoKwHLaEDL+pujxr3dzSOhyj9epvJ5DQqReTwU4GWPGvVUvt9b/+15MDhweeTFhQgGiFw1Mo2Mk9MH1hj5onC7lxZT/bEoo5cOl3v0G+gU6QFKA2VMAAAAASUVORK5CYII=" alt="Play" width="24" height="24"></a>');
    },
    error : function(request,error)
    {
        console.log("Request: "+JSON.stringify(request));
        console.log(error);
        location.reload(true);
    }
    });
});

  };

  $('.black').bind("enterKey",function(e){
     save();
    });

    $('.black').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});

  $input.one('blur', save).focus();

});

$('body').on('dblclick', '.command', function(){

  var $el = $(this);

  var index = $el.attr("class").split(" ")[1];

  var $input = $('<input class="black">').val( $el.text() );
  $el.replaceWith( $input );

  var save = function(){
    var $p = $('<div name="command ' + index + '" class="command ' + index + ' dots">').text( $input.val() );
    $input.replaceWith( $p );



         $(document).ready(function () {
    $.ajax({
    url : '/edit/' + index,
    type : 'PUT',
    contentType: 'application/json',
    async: true,
    data: JSON.stringify({ 'command': $input.val() }),
    success : function() {
        console.log("Updated!");
        $("td[id=" + index + "]").html('<a href="/start/' + index + '"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAABuElEQVRoQ9WZzVGFMBDH2VCJxUAyw4UOpATsADvQDrSD5zE37UA7eFagnslMHDLE8aCQkGx2H3dgf7v//UqguvAHtuzXWl9LKR85M+4B2KqqTkKIoWmaL44gIQCL3Z/W2kEp9cQNIhTA230nhJg4RSMWYAE5W2t7pdQbh2gcAfB2T1LKW2qIFIDF9ldjTN913TsVSCqAS3AAmNq2vaeAyAHg7SYptzkBSMptbgAXDWvtQ13XY4lyiwKwauoMAEPbti+YuYEJUKTclgBw5XYdRbI3v1IAaOW2JICTFAA8A0CfK8GLA6yJkW26pQLI1vyoAdx0m1JuOQD87BpSypvYnsEJ4FC55QbgK9UYOt2yBPDldp7nYW/XYAvg5ykp5dVWXnAGCNovOAJENTluAEFe/y0pLgBRXucGkHRYRhmBpBHCR4EKIMnrlBLK4nUqgGxeLw2Atg+7kWOrTWutlwuOlAf9ABgLANXr2BJC9zoKwHLaEDL+pujxr3dzSOhyj9epvJ5DQqReTwU4GWPGvVUvt9b/+15MDhweeTFhQgGiFw1Mo2Mk9MH1hj5onC7lxZT/bEoo5cOl3v0G+gU6QFKA2VMAAAAASUVORK5CYII=" alt="Play" width="24" height="24"></a>');
    },
    error : function(request,error)
    {
        console.log("Request: "+JSON.stringify(request));
        console.log(error);
        location.reload(true);
    }
      });
});

  };

  $('.black').bind("enterKey",function(e){
     save();
    });

    $('.black').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});

  $input.one('blur', save).focus();

});

$('body').on('click', '.channel', function(){

var $el = $(this);

if($el.hasClass("dots")){

$el.removeClass("dots");

}
else{

$el.addClass("dots");

}

});

$('body').on('click', '.command', function(){

var $el = $(this);

if($el.hasClass("dots")){

$el.removeClass("dots");

}
else{

$el.addClass("dots");

}

});