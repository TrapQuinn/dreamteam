index = 0;

$(document).ready(function(){

	$('#settings').on('submit','#confirmpassword', function() {
//		 var frm = $("#confirmpassword").serializeObject();
//		 var data = JSON.stringify(frm);

//		 console.log(data);

		    $.ajax({
		    url : '/checkpassword',
			async: true,
			type: "post",
			data: $("#confirmpassword").serialize(),
			//dataType : 'json',
			//contentType: "application/json",
			success: function(result){
			    index = result;
                $("#passwordvalidate").hide();
				$('#settings').find(".modal-body").html('<form id="changepassword" action="/changepassword/' + result + '" method="post" role="form"><br/><br/><div class="form-group  required"><label class="control-label" for="newpassword">New password</label><input class="form-control" id="newpassword" name="newpassword" placeholder="Enter your new password..." required="" type="password" value=""></div></input><br/><div class="form-group  required"><label class="control-label" for="confirmpassword">Confirm password</label><input class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm your new password..." required="" type="password" value=""></div><button type="submit" class="btn btn-lg btn-primary btn-block">Change password</button></form><div id="passwordvalidate" class="alert alert-danger alert-dismissible fade in"></div>');
	        },
	        error: function(xhr, status, error){
                var jsonResponse = JSON.parse(xhr.responseText);
	    		$("#passwordvalidate").html('<button type="button" id="close" class="close" data-hide="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>' + jsonResponse.message).show();
	        }
	        });


		return false;
	});

	});

$(document).ready(function(){

	$('#settings').on('submit','#changepassword', function() {

		    $.ajax({
		    url : '/changepassword/' + index,
			async: true,
			type: "post",
			data: $("#changepassword").serialize(),
			//dataType : 'json',
			//contentType: "application/json",
			success: function(result){
                $("#passwordvalidate").hide();
                alert("Password successfuly changed!");
                location.reload(true);
	        },
	        error: function(xhr, status, error){
                var jsonResponse = JSON.parse(xhr.responseText);
	    		$("#passwordvalidate").html('<button type="button" id="close" class="close" data-hide="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>' + jsonResponse.message).show();
	        }
	        });


		return false;
	});

	});

$(document).ready(function(){
	$('#settings').on('click','#close', function(){
        $("#passwordvalidate").hide();

        return false;
        // -or-, see below
        // $(this).closest("." + $(this).attr("data-hide")).hide();
    });
});

$(document).ready(function(){
$('#settings').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
});

});


