# This file defines command line commands for manage.py
#
# Copyright 2014 SolidBuilds.com. All rights reserved
#
# Authors: Ling Thio <ling.thio@gmail.com>

import datetime

from flask import current_app
from flask_script import Command

from app import db
from app.models import Employee, Role

class InitDbCommand(Command):
    """ Initialize the database."""

    def run(self):
        init_db()

def init_db():
    """ Initialize the database."""
    db.drop_all()
    db.create_all()
    create_users()


def create_users():
    """ Create users """

    # Create all tables
    db.create_all()

    # Adding roles
    admin_role = find_or_create_role('admin')

    # Add users
    user = find_or_create_user(u'Admin', u'Admin', u'Admin', u'admin@example.com', 'Password1', 1)
    user = find_or_create_user(u'Member', u'Member', u'Member', u'member@example.com', 'Password1')

    # Save to DB
    db.session.commit()


def find_or_create_role(name):
    """ Find existing role or create new role """
    role = Role.query.filter(Role.name == name).first()
    if not role:
        role = Role(name=name)
        db.session.add(role)
    return role


def find_or_create_user(username, first_name, last_name, email, password, role=None):
    """ Find existing user or create new user """
    user = Employee.query.filter(Employee.email == email).first()
    if not user:
        user = Employee(username=username,
                        email=email,
                        first_name=first_name,
                        last_name=last_name,
                        password=password)
        if role:
            user.role_id = role
        db.session.add(user)
    return user



