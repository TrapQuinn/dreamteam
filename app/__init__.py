# third-party imports
from flask import Flask
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import subprocess
from threading import Thread
from flask_user import UserManager, SQLAlchemyAdapter
#from flask_wtf.csrf import CSRFProtect
# local imports
from config import app_config

db = SQLAlchemy()
login_manager = LoginManager()
#csrf_protect = CSRFProtect()
mail = Mail()
migrate = Migrate()

list_of_running_pids = []
list_of_commands = []
list_of_retries = []
password_confirmed = []

def run_command(command,channel,id = -1):

    try:
        command_array = command.split()

        for idx, cmd in enumerate(command_array):
            command_array[idx] = cmd.replace('"', "")

        p = subprocess.Popen(command_array)

        if id == -1:
            list_of_running_pids.append(p)
            # status is 1(running)
            list_of_commands.append([command, channel, 1])
            list_of_retries.append(1)
        else:
            list_of_running_pids[id] = p
            list_of_commands[id][2] = 1

        print "Started process PID: " + str(p.pid)

        return
    except Exception as e:
        print e

    return

def run_process(id):

    command = list_of_commands[id][0]
    channel = list_of_commands[id][1]

    try:
        thread = Thread(target=run_command, args = (command,channel,id))
        thread.start()
    except Exception as e:
        print e
    print "thread finished...exiting"

def create_app(extra_config_settings={}):
    """Create a Flask applicaction.
    """
    # Instantiate Flask
    app = Flask(__name__)

    # Load App Config settings
    # Load common settings from 'app/settings.py' file
    app.config.from_object('app.settings')
    # Load local settings from 'app/local_settings.py'
    app.config.from_object('app.local_settings')
    # Load extra config settings from 'extra_config_settings' param
    app.config.update(extra_config_settings)

    # Setup Flask-Extensions -- do this _after_ app config has been loaded
    Bootstrap(app)
    # Setup Flask-SQLAlchemy
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)
    # Setup Flask-Migrate
    migrate.init_app(app, db)

    # Setup Flask-Mail
    mail.init_app(app)

    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    with app.app_context():

        try:
            if list_of_commands == []:
                commands = models.Command.query.all()
                db.session.commit()
                idx = 0
                for  x in commands:
                    list_of_commands.append([x.command, x.channel, 1])
                    list_of_running_pids.append(0)
                    list_of_retries.append(x.retries)
                    if list_of_retries[idx] == 1:
                        run_process(idx)
                    idx += 1

                print "Got all commands from database!"
        except Exception as e:
            print "Error getting commands from database " + str(e)
            raise


    # Setup WTForms CSRFProtect
    #csrf_protect.init_app(app)
    return app

'''
def create_app():
    app = Flask(__name__, instance_relative_config=True)
    #app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)

    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)
    return app
'''