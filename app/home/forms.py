# Forms for admin blueprint

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, validators, ValidationError
from wtforms.validators import DataRequired


class ExecutionForm(FlaskForm):
    """
    Form for users to enter command for execution
    """
    channel = StringField('Channel', validators=[DataRequired("Please enter your channel.")], render_kw={"placeholder": "Enter channel name here..."})
    command = StringField('Command',validators=[DataRequired("Please enter your command.")], render_kw={"placeholder": "Enter your command here..."})
    submit = SubmitField('Execute')

class PasswordCheckForm(FlaskForm):

    password = PasswordField('Current Password', validators=[DataRequired("Please enter your current password.")], render_kw={"placeholder":"Enter your current password..."})