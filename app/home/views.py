from flask import render_template, request, Response, url_for, redirect, json, flash
from flask_login import login_required, current_user, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy
from . import home
from forms import ExecutionForm, PasswordCheckForm
import subprocess
import json
import sys
from time import gmtime, strftime, sleep
from threading import Thread
from ..models import db
from ..models import Employee, Command
from app import list_of_commands, list_of_retries, list_of_running_pids, password_confirmed

def get_all_commands():
    try:
        if list_of_commands == []:
            commands = Command.query.all()
            db.session.commit()

            for x in commands:
                list_of_commands.append([x.command, x.channel, 1])
                list_of_running_pids.append(0)
                list_of_retries.append(x.retries)

            print "Got all commands from database!"
    except Exception as e:
        print "Error getting commands from database " + str(e)
        raise

    return

def commit_command(command,channel,retries):
    try:
        command = Command(command=command,channel=channel,retries=retries)

        # add employee to the database
        db.session.add(command)
        db.session.commit()

        print "Command added to database"

    except Exception as e:
        print "Error adding command to database! " + str(e)

    return

def update_command(command,new_command):
    try:

        command = Command.query.filter_by(command=command).first()
        command.command = new_command
        db.session.commit()

        print "Command successfully updated!"
    except Exception as e:
        print "Error updating command in database" + str(e)

    return

def update_channel(command,new_channel):
    try:

        channel = Command.query.filter_by(command=command).first()
        channel.channel = new_channel
        db.session.commit()

        print "Channel successfully updated!"
    except Exception as e:
        print "Error updating channel in database" + str(e)

    return

def update_retries(command,retries):
    try:

        status = Command.query.filter_by(command=command).first()
        status.retries = retries
        db.session.commit()

        print "Retries succesfully updated!"
    except Exception as e:
        print "Error updating retries in database " + str(e)

    return

def delete_command(command):
    try:

        channel = Command.query.filter_by(command=command).first()
        db.session.delete(channel)
        db.session.commit()

        print "Command successfully deleted!"
    except Exception as e:
        print "Error deleting command in database " + str(e)

    return

def run_command(command,channel,id = -1):

    try:
        command_array = command.split()

        for idx, cmd in enumerate(command_array):
            command_array[idx] = cmd.replace('"', "")

        p = subprocess.Popen(command_array)

        if id == -1:
            list_of_running_pids.append(p)
            # status is 1(running)
            list_of_commands.append([command, channel, 1])
            list_of_retries.append(1)
        else:
            list_of_running_pids[id] = p
            list_of_commands[id][2] = 1

        print "Started process PID: " + str(p.pid)

        return
    except Exception as e:
        print e

    return

def check_status(p):
    """
    A None value indicates that the process hasn't terminated yet.
    """
    try:
        if p == 0:
            return 0
        poll = p.poll()
        if poll == None:
            return 1
        return 0
    except Exception as e:
        print e

@home.route('/')
def homepage():
    """
    Render the homepage template on the / route
    """

    form = PasswordCheckForm()

    return render_template('home/index.html', passwordform=form, title="Welcome")


@home.route('/dashboard')
@login_required
def dashboard():
    """
    Render the dashboard template on the /dashboard route
    """
    form = ExecutionForm()
    passwordform = PasswordCheckForm()
    try:
        get_all_commands()
    except Exception as e:
        print e

    return render_template('home/dashboard.html', passwordform = passwordform, form=form, list_of_commands=list_of_commands, title="Dashboard")

@home.route('/process', methods=['POST'])
@login_required
def create_process():
    form = ExecutionForm(request.form)
    command = form.command.data
    channel = form.channel.data

    command_array = command.split()
    try:
        for idx, cmd in enumerate(command_array):
            command_array[idx] = cmd.replace('"', "")

        p = subprocess.Popen(command_array)

        p.kill()

        commit_command(command, channel, 1)
    except Exception as e:
        print e

    try:
        thread = Thread(target=run_command, args = (command,channel))
        thread.start()
    except Exception as e:
        print e
    print "thread finished...exiting"
    return redirect(url_for('home.dashboard'))

@home.route('/start/<int:id>', methods=['GET'])
@login_required
def start_process(id):

    list_of_retries[id] = 1

    command = list_of_commands[id][0]
    channel = list_of_commands[id][1]

    update_retries(command,1)

    try:
        thread = Thread(target=run_command, args = (command,channel,id))
        thread.start()
    except Exception as e:
        print e
    print "thread finished...exiting"
    return redirect(url_for('home.dashboard'))

def rerun_process(id):

    command = list_of_commands[id][0]
    channel = list_of_commands[id][1]

    try:
        thread = Thread(target=run_command, args = (command,channel,id))
        thread.start()
    except Exception as e:
        print e
    print "thread finished...exiting"
    return redirect(url_for('home.dashboard'))

@home.route('/edit/<int:id>', methods=['PUT'])
@login_required
def edit_data(id):

    try:
        stop_process(id)
        data = request.data
        commands = json.loads(data)

        for key, value in commands.iteritems():

            if key == "channel":
                try:
                    update_channel(list_of_commands[id][0], value)
                except Exception as e:
                    print e
                list_of_commands[id][1] = value
            else:
                try:
                    update_command(list_of_commands[id][0], value)
                except Exception as e:
                    print e
                list_of_commands[id][0] = value


    except Exception as e:
        print e

    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}

@home.route('/<int:id>/status', methods=['GET'])
@login_required
def process_status(id):

    try:
        try:
            p = list_of_running_pids[id]

            list_of_commands[id][2] = check_status(p)

        except Exception as e:
            print e
            list_of_commands[id][2] = 0

        if list_of_commands[id][2] == 0:
            list_of_running_pids[id] = 0

            if list_of_retries[id] != 0:

                try:
                    if list_of_running_pids[id] == 0:
                        rerun_process(id)
                except Exception as e:
                    print e
                    list_of_retries[id] = 0
    except Exception as e:
        print e

    for i in list_of_running_pids:
        try:
            print i.pid
        except Exception as e:
            print e

    response = app.response_class(
        response=json.dumps([list_of_commands[id][2],list_of_retries[id],list_of_commands[id][0],list_of_commands[id][1]]),
        status=200,
        mimetype='application/json'
    )

    return response

@home.route('/kill/<int:id>', methods=['GET'])
@login_required
def kill_process(id):
    try:
        print "Killed PID of id: " + str(list_of_running_pids[id])
        list_of_running_pids[id].kill()
        del list_of_running_pids[id]
        try:
            delete_command(list_of_commands[id][0])
        except Exception as e:
            print e
        del list_of_commands[id]
        del list_of_retries[id]
    except:
        print("Oops!", sys.exc_info()[0], "occured.")
        if (len(list_of_running_pids) - 1) >= id:
            del list_of_running_pids[id]
            try:
                delete_command(list_of_commands[id][0])
            except Exception as e:
                print e
            del list_of_commands[id]
            del list_of_retries[id]
        pass

    return redirect(url_for('home.dashboard'))

@home.route('/stop/<int:id>', methods=['GET'])
@login_required
def stop_process(id):
    try:
        print "Stopped PID: " + str(list_of_running_pids[id].pid)
        list_of_running_pids[id].kill()
        list_of_running_pids[id] = 0
        list_of_commands[id][2] = 0
        #The number 0 means it is stopped by hand
        list_of_retries[id] = 0

        update_retries(list_of_commands[id][0],0)
    except:
        try:
            print("Oops!", sys.exc_info()[0], "occured.")
            list_of_running_pids[id] = 0
            list_of_commands[id][2] = 0
            #The number 0 means it is stopped by hand
            list_of_retries[id] = 0
            update_retries(list_of_commands[id][0],0)
        except Exception as e:
            print("Oops!", sys.exc_info()[0], "occured.")
        pass
    return redirect(url_for('home.dashboard'))

@home.route('/checkpassword', methods=['POST'])
@login_required
def check_password():
    try:
        form = PasswordCheckForm(request.form)
        # check whether employee exists in the database and whether
        # the password entered matches the password in the database
        employee = Employee.query.filter_by(id=current_user.id).first()
        if employee is not None and employee.verify_password(form.password.data):

            password_confirmed.append([current_user.id,True])

            # redirect to the dashboard page after login
            response = app.response_class(
                response=json.dumps(len(password_confirmed)-1),
                status=200,
                mimetype='application/json'
            )
            return response
    except Exception as e:
        print e

    # when login details are incorrect

    response = app.response_class(
        response=json.dumps({"message":"Wrong password!"}),
        status=401,
        mimetype='application/json'
    )

    return response


@home.route('/changepassword/<int:id>', methods=['POST'])
@login_required
def change_password(id):
    try:
        passwords = request.form
        if passwords['newpassword'] != passwords['confirmpassword']:
            response = app.response_class(
                response=json.dumps({"message": "Passwords are not same!"}),
                status=401,
                mimetype='application/json'
            )
            return response

        employee = Employee.query.filter_by(id=current_user.id).first()
        if employee is not None:

            if password_confirmed[id][1] == True:

                employee.password = passwords['newpassword']
                db.session.commit()

                password_confirmed[id][1] = False

                # redirect to the dashboard page after login
                response = app.response_class(
                    response=json.dumps(password_confirmed),
                    status=200,
                    mimetype='application/json'
                )
                return response
            else:
                response = app.response_class(
                    response=json.dumps({"message":"You didnt confirmed your current password!"}),
                    status=401,
                    mimetype='application/json'
                )
                return response

        # when login details are incorrect
    except Exception as e:
        print e

    response = app.response_class(
        response=json.dumps({"message":"Could not find that user!"}),
        status=404,
        mimetype='application/json'
    )
    return response

from flask import Flask
app = Flask(__name__)