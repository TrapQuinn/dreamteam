# Clone the code repository into ~/dev/my_app
    mkdir -p ~/dev
    cd ~/dev
    git clone <repo> my_app

    # Create the 'my_app' virtual environment
    virtualenv -p PATH/TO/PYTHON my_app

    # Install required Python packages
    cd ~/dev/my_app

    pip install -r requirements.txt

    python manage.py init_db
    
    after init_db go to: Start as service in background section.
    This is command for direct start, used just for testing (python manage.py runserver &)

    You can make use of the following users:
- email `user@example.com` with password `Password1`.
- email `admin@example.com` with password `Password1`.

# Nginx setup:

- Install ngix on ubuntu (apt-get install nginx)
- cd ~/etc/nginx/conf.d/
- vi proxy500.conf 
- add following code to proxy500.conf file
```
server {
        listen 80;
        listen [::]:80;


    location / {
       proxy_pass http://127.0.0.1:5000;
    }
}
```
- type following comand to unlink default config: unlink /etc/nginx/sites-enabled/default
- /etc/init.d/nginx restart

#Start as service in background:

- Service also starts on boot of the machine.

- Go to the directory where manage.py file is located. Then make sure this is executable with:
    ```
    sudo chmod +x manage.py
    ```
- After entering previous command, now enter command:
    ```
    cd
    ```
- That command is followed by command, which brings us into root directory:
    ```
    cd ..
    ```
- Now type this command:
    ```
    cd /lib/systemd/system
    ```
- Install nano, if not availabe with
    ```
    apt-get install nano
    ```
- Create manage.service file with command:
    ```
    nano manage.service
    ```
- Copy and paste code below into opened manage.service:

```
    [Unit]
    Description=Manage processes for ffmpeg
    After=multi-user.target

    [Service]
    ExecStart=/usr/bin/python /var/bin/dreamteam/manage.py runserver
    StandardOutput=null

    [Install]
    WantedBy=multi-user.target
    Alias=manage.service
```

- After this part you can start service with:

```
    systemctl start manage.service
```
    or
    
```
    sudo systemctl start manage.service
```

- Also you can restart service with:
    
```
    systemctl restart manage.service
```

or

stop service with:

```
    systemctl stop manage.service
```


